# Some frontend projects I have built, coded and launched



## Crafata (Mobile IOS and Android Apps and Websites)
### IOS and Android Apps launched in Google Play store and IOS App Store within 4 months for 1st version. Using react native
<img src="/media/crafata/preview1.png" alt="My cool logo" width="30%"/>
<img src="/media/crafata/preview2.png" alt="My cool logo" width="30%"/>
<img src="/media/crafata/preview3.png" alt="My cool logo" width="30%"/>
<img src="/media/crafata/preview4.png" alt="My cool logo" width="30%"/>
<img src="/media/crafata/preview5.png" alt="My cool logo" width="30%"/>

### Websites using reactjs
#### homepage
<img src="/media/crafata/website2.png" alt="My cool logo" width="70%"/>

#### user management page
<img src="/media/crafata/website1.png" alt="My cool logo" width="70%"/>



## Huainor
### Andorid Apps and Wechat mini program(javascript based) 
<img src="/media/huainor/app.png" alt="My cool logo" width="30%"/>

### websites
#### search engine
<img src="/media/huainor/video_gif.gif" alt="My cool logo" width="70%"/>

#### CRM 
<img src="/media/huainor/tax_agent.gif" alt="My cool logo" width="70%"/>

